//导入第三方模块
var createError = require("http-errors")
var express = require("express")
var path = require("path")
var cookieParser = require("cookie-parser")
var logger = require("morgan")
var cors = require("cors")
//导入自定义模块
var indexRouter = require("./routes/index")
var usersRouter = require("./routes/users")
 
//创建experss对象
var app = express()
//配置跨域中间件
app.use(cors())
 
// view engine setup
app.set("views", path.join(__dirname, "views")) //更改视图模板路径
app.set("view engine", "ejs") //配置视图引擎
 
//配置中间件
app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public"))) //配置express服务器下静态资源目录
app.use("/upload", express.static(path.join(__dirname, "upload"))) //配置express服务器下上传文件资源目录
//参数一 /upload  为 要显示的图片的路由前缀
 
//注册自定义路由模块
app.use("/", indexRouter)
app.use("/users", usersRouter)
 
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  //当运行出错了，会自动捕获404页面
  next(createError(404))
})
 
// error handler
app.use(function (err, req, res, next) {
  console.log(err.message)
  //全局错误处理中间件 当程序出现错误 会执行该中间件
  // set locals, only providing error in development
  // res.locals.message = err.message
  // res.locals.error = req.app.get("env") === "development" ? err : {}
 
  // // render the error page
  // res.status(err.status || 500)
  // res.render("error")
})
 
module.exports = app