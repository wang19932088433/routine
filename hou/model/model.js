let mongoose = require("./db");

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

const carSchema = new mongoose.Schema({
  school: String,
  num: Number,
  path: String,
  name: String,
  price: Number,
});

const tuiSchema = new mongoose.Schema({
  school: String,
  num: Number,
  path: String,
  name: String,
  price: Number,
});

var countSchema = new mongoose.Schema({
  count: Number,
  src: String,
  title: String,
  time: Date,
});

var countModel = mongoose.model("count", countSchema, "count");
let userModel = mongoose.model("user", userSchema, "user");
let carModel = mongoose.model("car", carSchema, "car");
let tuiModel = mongoose.model("tui", tuiSchema, "tui");

module.exports = { userModel, carModel, tuiModel, countModel };
