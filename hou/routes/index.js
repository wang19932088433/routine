var express = require('express');
var router = express.Router();
var { carModel, tuiModel,userModel,countModel } = require('../model/model')
// const alipaydev = require('../zhi/db.js');
const alipaySdk = require('../zhi/db');
const AlipayFormData = require('alipay-sdk/lib/form');

//列表
router.get("/car", async (req, res) => {
    let data = await carModel.find()
    res.send(data)
})

router.get("/tui", async (req, res) => {
    let data = await tuiModel.find()
    res.send(data)
})

router.get("/xiang", async (req, res) => {
    let id = req.query.id
    let data = await carModel.find({ _id: id })
    res.send({
        code: 200,
        msg: '查询成功',
        data: data
    })
    console.log(id);
})

router.get("/xian", async (req, res) => {
    let id = req.query.id
    let data = await tuiModel.find({ _id: id })
    res.send({
        code: 200,
        msg: '查询成功',
        data: data
    })
    // console.log(id);
})

router.post('/zhi',  (req, res) => {
    let {orderId,price,name} = req.body
    console.log(orderId);
    const formData = new AlipayFormData();

    formData.setMethod('get');

    formData.addField('returnUrl', `https://www.baidu.com/`);

    formData.addField('bizContent', {
        outTradeNo: orderId,
        productCode: 'FAST_INSTANT_TRADE_PAY',
        totalAmount: price,
        subject: name,
        body: '测试订单描述',
    });

    const result =  alipaySdk.exec(
        'alipay.trade.page.pay',
        {},
        { formData: formData },
    )

    result.then((resp) => {
        res.send({
            success: 'true',
            code: 200,
            'result': resp
        })
    })
})



//用户列表
router.get("/user_list",async (req,res)=>{
    let data=await userModel.find()
    res.send(data)
})
//用户添加
router.post("/user_create",async (req,res)=>{
    let data=req.body
    let user=new userModel(data)
    let result=await user.save()
    res.send({

        msg:"user created",
        data:result
    })
})

//获取折扣信息
router.get('/getCount', async function(req, res, next) {
    let countLst = await countModel.find()
    // console.log(countLst);
    res.send({
      code:200,
      countLst,
    })
  })

module.exports = router;
