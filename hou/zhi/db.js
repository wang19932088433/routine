const AlipaySdk = require('alipay-sdk').default;

const alipaySdk = new AlipaySdk({
    appId: '2021004143601732',//appid
    signType:'RSA2',//签名类型
    gateway:'https://openapi-sandbox.dl.alipaydev.com/gateway.do',//支付宝网关
    //公钥
    alipayPublicKey:'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhibgOgupPhN3EyYgcsqd7VooKiPjhRcHGVz2zfNZOWhtSigPIggnKE74x3nwra9gKLfpDj922mbzty4WNzTj6NN64ER54Ub++SoJCDRqbkA+YjPlqIwcHmvI3J0nb85ZpBYIH5ys3yr5CL2AqL/g+nCwgYBT/zkJX3zaeWjeJL/B4OkCLfSJx6pU3UmIcNaaVowgS/J/GS4Brxo8WmaAtTBVahhsLdwEBt/EswGPN3C1svJIXVdwEAq2fFreVrrgZ/mxMHCyChsoyrccXM1kcYLaPBqjae9DrMm/mKV5D/0NQMm93quDoYrZZ+wd49K/QqizFRvmuXrOML7RszxfdQIDAQAB',
    //私钥
    privateKey:'MIIEowIBAAKCAQEArmXMK9MIXu823KFTmOiTIieAd26x7mrAVBExO/TvAtqLw4vSQ/bYl/s1mSe5M63de/Yc86Ct21ykPyMGRALjZofk6N8Sv3iQrlaNuWSCEZSllEFPWBycakSr1+NzZT7H0Ool6lXRxjXTw/ibMiBg/b/BTKLlCa6YBHCIq+6qx6DRwT9lEc+igaq9++oxUxESu6ttto/8hWaYV1LfvTvHKe/tt9NXDuIiBHIqTfJvHphV+cJmBEul2tGv7p8p56/TunwF76EvqwZJFDVXFbPIquUMzmDyXKbLKnNF1TPcw08wGdMKB/yGhWHLa2EBHFJ6YD20jxWGd/DKU0GPSPJVmQIDAQABAoIBAG5PC1aI9BmAO3ZQQJ1ONop66+IfTUJdmWtTOI4Q/gETFSZJ0WSVxShUh/Fo09chWR+oa2WaM3212lnpIHRyUz2uJRc6yhP8AZ+UNa6XKUJPgHAG41X7OuBosT0rq/jVPfQhtnvDbp1tvJWPMTMCkfbOjEiaHD8EhTkBqSD/JFyBi1YsJn6GJkgIQXnhn0NhVUrkOZfohSiBQTnuRM7+iYyQkfGf6NwzvZ6C6HOiipJso4JA6y7VHvg/BbFQU4tsanDsDTBXXLmwSjug+YFpNbVVir4rOOKpCc+/gfD+xrNoAxPmusFKRn66H5WpGEZ8VFWxG7RWP4P40gnMymhDViUCgYEA4qik/u9TVIGCTDvRR8lNzkZjrERVcXzh/Uo+AeX+n/Pux2xf1oxqQBUD0Jg1LHmKGr21wlntbosV55w8JndL7VZakZ02NEgtnbBPKH3smK2gB7P1iPkw8DeD7hh9mAfoP7tDcu9El1uY8wz9KiGzg+4/SkJ+QDcLbVM2rI7ypkcCgYEAxPk/CcX+qXLkdZ2RjABedTOH6vh371zv1ExkM2jCnLiQRhRm9MpmFdvWrR16bQwO6tP5b7xf2QAkPPc9bd8+ZV3yH5VKcrl0L4gRmjBuFYxFmAMFKr5GKFy/Mvn58xODG/6tvFxEjKL3RAliRxg8WDMnVTAgpzENpBQKV0YAtR8CgYBhcV1V2G/7wZ3Rxlmnwgx4JNzO/6W4H5dMfHGCAW3hOE6sX5QoOb+oAyDbucQKdcoAyxRsFJs1zgfBxfI6G0mCJIDVX0AhY/Jwe47cep6P2P0WsrBZjcATVdqbLt430RZw58IMlH7pj1CHFUt1pH13t6Jv+3wpgUChZIf50Y7SGwKBgALABBxL0mFlREFMIOMWgjsarBQgwfPqlUKeimuZU9ZCUWlZEZLrXH+DoTyrg7EQEQlASmQ/+gqakOb1ALuO+V0K8b11JSPy6Odc+/nuIStV2QblS+Yr3+jiWp0Ilm/JXvQ+GwjdNOvNvqwJgYDCmt0UYcEzudD7w1f+G6LaTFo7AoGBAKZTzVvspMqroatCYwGOKy+csn3vTVtZQ6LZbGKYmK13uqzXBcFtJW7HPaZkC4M9xDco6QrlS9NPidyPmCCSSh4rTHu9ujCrImbdEDEKcx1Q04D2UciKnmo96+7T0DtrWFxNaU8wMKu+wRmrSuoJjG8KZO2cpY2E52F43lx7O3xO'
})

module.exports=alipaySdk