// en.ts
const locale = {
  index: {
    title: "home"
  },
  order:{
  	title: "order"
  },
  tabBar: {
  	home: 'home',
  	order: 'order',
  	my: 'my'
  },
  my:{
	myTitle:'my',
	myItem:{
		coupon:"coupon",
		myInvoice:"myInvoice",
		serviceCenter:"serviceCenter",
		option:"option",
		join:"Join Us"
	},
	couponItem:{
		title:'coupon',
		getTime:'getTime',
	},
  	optionTitle:'option',
	changeLangTitle:'changeLanguage',
	languageOption:{
		engLish:'EngLish',
		simple:'simpleChinese',
	},
	option:{
		language:'language',
	}
  }
}

export default locale

