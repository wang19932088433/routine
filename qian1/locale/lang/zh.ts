// zh.ts
const locale = {
  index: {
    title: "首页"
  },
  order:{
	title: "订单"
  },
  tabBar: {
	home: '首页',
	order: '订单',
	my: '我的'
  },
  my:{
	myTitle:'我的',
	myItem:{
		coupon:"优惠券",
		myInvoice:"我的发票",
		serviceCenter:"客服中心",
		option:"设置",
		join:"加入成为车主"
	},
	couponItem:{
		title:'优惠券',
		getTime:'申请日期',
	},
  	optionTitle:'操作',
	changeLangTitle:'语言切换',
	languageOption:{
		engLish:'英文',
		simple:'简体中文',
	},
	option:{
		language:'语言',
	},
  }
}

export default locale
