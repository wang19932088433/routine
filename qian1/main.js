import App from './App'


// #ifndef VUE3
import Vue from 'vue';
import './uni.promisify.adaptor';
import uviewPlus from '@/uni_modules/uview-plus'
Vue.use(uviewPlus)
Vue.config.productionTip = false
App.mpType = 'app'



const app = new Vue({
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import Vuei18n from './locale/index.js'
import { createPinia } from 'pinia'


	
export function createApp() {
  const app = createSSRApp(App)

  app.use(Vuei18n)
  app.use(createPinia())
  return {
    app
  }
}
// #endif