import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useI18nStore = defineStore('i18n', () => {
  const currentLanguage = ref('zh'); // 默认语言
  const texts = {
    zh: {
      home: '首页',
      order: '订单',
      my: '我的'
    },
    en: {
      home: 'home',
      order: 'order',
      my: 'my'
    }
  };

  function switchLanguage(lang: string) {
    currentLanguage.value = lang;
    // 触发TabBar更新
    updateTabBarTexts();
  }

  function updateTabBarTexts() {
    const tabBarTexts = texts[currentLanguage.value];
    // 这里假设你有方法可以全局更新TabBar
    // 实际应用中，你可能需要通过事件总线、Vuex或Pinia的action来通知全局组件更新
    console.log('Updating TabBar texts:', tabBarTexts);
  }

  return { currentLanguage, switchLanguage, updateTabBarTexts };
});