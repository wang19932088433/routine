"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
const locale_index = require("./locale/index.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/login/login.js";
  "./pages/my/my.js";
  "./pages/my/coupon/Coupon.js";
  "./pages/my/myInvoice/myInvoice.js";
  "./pages/my/serviceCenter/serviceCenter.js";
  "./pages/my/options/options.js";
  "./pages/my/language/language.js";
  "./pages/cart/cart.js";
  "./pages/changeCity/changeCity.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "D:/a-django/专高六2/routine/qian1/App.vue"]]);
function createApp() {
  const app = common_vendor.createSSRApp(App);
  app.use(locale_index.i18n);
  app.use(common_vendor.createPinia());
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
