"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _component_van_icon = common_vendor.resolveComponent("van-icon");
  const _component_van_tab = common_vendor.resolveComponent("van-tab");
  const _component_van_tabs = common_vendor.resolveComponent("van-tabs");
  (_component_van_icon + _component_van_tab + _component_van_tabs)();
}
const _sfc_main = {
  __name: "cart",
  setup(__props) {
    const active = common_vendor.ref(0);
    const xiangqing = () => {
      common_vendor.index.navigateTo({ url: "/pages/Orderdetail/Orderdetail" });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          name: "delete-o"
        }),
        b: common_vendor.p({
          name: "delete-o"
        }),
        c: common_vendor.o(($event) => xiangqing(_ctx.id)),
        d: common_vendor.p({
          name: "delete-o"
        }),
        e: common_vendor.p({
          title: "全部"
        }),
        f: common_vendor.p({
          name: "delete-o"
        }),
        g: common_vendor.p({
          title: "快车"
        }),
        h: common_vendor.p({
          name: "delete-o"
        }),
        i: common_vendor.p({
          title: "拼车"
        }),
        j: common_vendor.p({
          name: "delete-o"
        }),
        k: common_vendor.p({
          title: "顺风车"
        }),
        l: common_vendor.p({
          name: "delete-o"
        }),
        m: common_vendor.p({
          title: "租车"
        }),
        n: common_vendor.o(($event) => active.value = $event),
        o: common_vendor.p({
          sticky: "true",
          titleActiveColor: "#569CD6",
          color: "#569CD6",
          textColor: "#f6f3f3",
          active: active.value
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "D:/a-django/专高六2/routine/qian1/pages/cart/cart.vue"]]);
wx.createPage(MiniProgramPage);
