"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_up_navbar2 = common_vendor.resolveComponent("up-navbar");
  const _easycom_up_search2 = common_vendor.resolveComponent("up-search");
  const _easycom_up_icon2 = common_vendor.resolveComponent("up-icon");
  const _component_van_sidebar_item = common_vendor.resolveComponent("van-sidebar-item");
  const _component_van_sidebar = common_vendor.resolveComponent("van-sidebar");
  (_easycom_up_navbar2 + _easycom_up_search2 + _easycom_up_icon2 + _component_van_sidebar_item + _component_van_sidebar)();
}
const _easycom_up_navbar = () => "../../uni_modules/uview-plus/components/u-navbar/u-navbar.js";
const _easycom_up_search = () => "../../uni_modules/uview-plus/components/u-search/u-search.js";
const _easycom_up_icon = () => "../../uni_modules/uview-plus/components/u-icon/u-icon.js";
if (!Math) {
  (_easycom_up_navbar + _easycom_up_search + _easycom_up_icon)();
}
const _sfc_main = {
  __name: "changeCity",
  setup(__props) {
    let city = common_vendor.ref("保定");
    common_vendor.ref(void 0);
    common_vendor.ref(1);
    common_vendor.ref(3);
    const keyword = common_vendor.ref("");
    let activeKey = common_vendor.ref(0);
    let provinceList = common_vendor.reactive([
      {
        name: "热门城市",
        cityList: ["邯郸市", "唐山市", "保定市", "杭州市"]
      },
      {
        name: "北京市",
        cityList: ["通州区", "朝阳区", "丰台区"]
      },
      {
        name: "上海市",
        cityList: ["上海市"]
      },
      {
        name: "广东省",
        cityList: ["广州市", "深圳市", "珠海市", "汕头市", "佛山市"]
      },
      {
        name: "浙江省",
        cityList: ["杭州市", "宁波市", "温州市", "嘉兴市", "绍兴市"]
      },
      {
        name: "江苏省",
        cityList: ["南京市", "苏州市", "无锡市", "常州市", "徐州市"]
      },
      {
        name: "天津市",
        cityList: ["天津市"]
      },
      {
        name: "河北省",
        cityList: ["石家庄市", "唐山市", "秦皇岛市", "邯郸市", "邢台市", "保定市", "张家口市", "承德市", "沧州市", "廊坊市", "衡水市"]
      },
      {
        name: "山西省",
        cityList: ["太原市", "大同市", "阳泉市", "长治市", "晋城市", "朔州市", "晋中市", "运城市", "忻州市", "临汾市", "吕梁市"]
      },
      {
        name: "四川省",
        cityList: [
          "成都市",
          "自贡市",
          "攀枝花市",
          "泸州市",
          "德阳市",
          "绵阳市",
          "广元市",
          "遂宁市",
          "内江市",
          "乐山市",
          "南充市",
          "眉山市",
          "宜宾市",
          "广安市",
          "达州市",
          "雅安市",
          "巴中市",
          "资阳市",
          "阿坝藏族羌族自治州",
          "甘孜藏族自治州",
          "凉山彝族自治州"
        ]
      }
    ]);
    const changeCity = (index) => {
      activeKey.value = index;
    };
    const changeUrl = (item) => {
      city.value = item;
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          title: "修改城市",
          autoBack: true
        }),
        b: common_vendor.o(($event) => keyword.value = $event),
        c: common_vendor.p({
          placeholder: "搜索城市",
          modelValue: keyword.value
        }),
        d: common_vendor.p({
          name: "map-fill",
          color: "#aaa",
          size: "25"
        }),
        e: common_vendor.t(common_vendor.unref(city)),
        f: common_vendor.p({
          name: "reload",
          color: "#aaa",
          size: "25"
        }),
        g: common_vendor.f(common_vendor.unref(provinceList), (item, index, i0) => {
          return {
            a: item.name,
            b: common_vendor.o(($event) => changeCity(index), item.name),
            c: "19a4f5ae-5-" + i0 + ",19a4f5ae-4",
            d: common_vendor.p({
              title: item.name,
              name: index
            })
          };
        }),
        h: common_vendor.p({
          activeKey: common_vendor.unref(activeKey)
        }),
        i: common_vendor.f(common_vendor.unref(provinceList)[common_vendor.unref(activeKey)].cityList, (item, index, i0) => {
          return {
            a: common_vendor.t(item),
            b: item,
            c: common_vendor.n(index % 2 === 0 ? "con" : "con cons"),
            d: common_vendor.o(($event) => changeUrl(item), item)
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "D:/a-django/专高六2/routine/qian1/pages/changeCity/changeCity.vue"]]);
wx.createPage(MiniProgramPage);
