"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_up_icon2 = common_vendor.resolveComponent("up-icon");
  _easycom_up_icon2();
}
const _easycom_up_icon = () => "../../uni_modules/uview-plus/components/u-icon/u-icon.js";
if (!Math) {
  _easycom_up_icon();
}
const citySelector = requirePlugin("citySelector");
const chooseLocation = requirePlugin("chooseLocation");
let city = common_vendor.ref("保定");
let latitude = common_vendor.ref(38.814119268419994);
let longitude = common_vendor.ref(115.49178149073794);
let startGps = common_vendor.reactive({});
let endGps = common_vendor.reactive({});
let type = common_vendor.ref("");
common_vendor.ref(false);
common_vendor.reactive(["快车", "拼车", "专车", "特快"]);
const __default__ = {
  onLoad() {
    console.log("页面加载");
  },
  onShow() {
    const selectedCity = citySelector.getCity();
    const selectedChoose = chooseLocation.getLocation();
    console.log(selectedChoose);
    if (selectedCity) {
      console.log(selectedCity.name);
      latitude.value = selectedCity.location.latitude;
      longitude.value = selectedCity.location.longitude;
      city.value = selectedCity.name;
    }
    if (selectedChoose) {
      if (type.value == "出发地") {
        Object.assign(startGps, {
          name: selectedChoose.name,
          latitude: selectedChoose.latitude,
          longitude: selectedChoose.longitude
        });
        common_vendor.index.setStorageSync("startGps", JSON.stringify(startGps));
      } else if (type.value == "目的地") {
        Object.assign(endGps, {
          name: selectedChoose.name,
          latitude: selectedChoose.latitude,
          longitude: selectedChoose.longitude
        });
        common_vendor.index.setStorageSync("endGps", JSON.stringify(endGps));
        type.value = "";
        common_vendor.index.navigateTo({
          url: "/pages/selectCar/selectCar"
        });
      }
    }
  }
};
const _sfc_main = /* @__PURE__ */ Object.assign(__default__, {
  __name: "index",
  setup(__props) {
    let covers = common_vendor.ref([
      {
        id: 1,
        longitude: 115.49709226458742,
        latitude: 38.815782835727525,
        // 图片标记点
        iconPath: "../../static/asstes/汽车l.png",
        width: 25,
        height: 25
      },
      {
        id: 2,
        longitude: 115.49715663760378,
        latitude: 38.81686956725767,
        // 图片标记点
        iconPath: "../../static/asstes/汽车l.png",
        width: 25,
        height: 25
      },
      {
        id: 3,
        longitude: 115.49443151324465,
        latitude: 38.81686956725767,
        // 图片标记点
        iconPath: "../../static/asstes/汽车l.png",
        width: 25,
        height: 25
      },
      {
        id: 4,
        longitude: 115.49687768786623,
        latitude: 38.81359260333601,
        // 图片标记点
        iconPath: "../../static/asstes/汽车l.png",
        width: 25,
        height: 25
      },
      {
        id: 5,
        longitude: 115.49584771960451,
        latitude: 38.81488000000115,
        // 图片标记点
        iconPath: "../../static/asstes/汽车l.png",
        width: 25,
        height: 25
      }
    ]);
    function getCurrentLocation() {
      common_vendor.index.getLocation({
        type: "gcj02",
        success: (res) => {
          latitude.value = res.latitude;
          longitude.value = res.longitude;
        }
      });
    }
    function mapChooseLocation(types) {
      type.value = types;
      const info = {
        lat: latitude.value,
        lng: longitude.value
      };
      const key = "N2ABZ-7ZNC5-74BIK-IID4H-M5FG2-YTFPO";
      const referer = "出行App";
      const location = JSON.stringify({
        latitude: Number(info.lat),
        longitude: Number(info.lng)
      });
      const category = "";
      common_vendor.wx$1.navigateTo({
        url: "plugin://chooseLocation/index?key=" + key + "&referer=" + referer + "&location=" + location + "&category=" + category
      });
    }
    function mapSelectCity() {
      const info = {
        lat: latitude.value,
        lng: longitude.value
      };
      const key = "N2ABZ-7ZNC5-74BIK-IID4H-M5FG2-YTFPO";
      const referer = "出行App";
      JSON.stringify({
        latitude: Number(info.lat),
        longitude: Number(info.lng)
      });
      common_vendor.wx$1.navigateTo({
        url: `plugin://citySelector/index?key=${key}&referer=${referer}`
      });
    }
    common_vendor.onBeforeMount(() => {
      getCurrentLocation();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(common_vendor.unref(city)),
        b: common_vendor.o(mapSelectCity),
        c: common_vendor.p({
          name: "arrow-down-fill",
          color: "#fff",
          size: "12"
        }),
        d: common_vendor.unref(latitude),
        e: common_vendor.unref(longitude),
        f: common_vendor.unref(covers),
        g: common_vendor.o((...args) => _ctx.handleRegionChange && _ctx.handleRegionChange(...args)),
        h: common_vendor.t(common_vendor.unref(startGps).name ? common_vendor.unref(startGps).name : "莲池区科技学院"),
        i: common_vendor.o(($event) => mapChooseLocation("出发地")),
        j: common_vendor.t(common_vendor.unref(endGps).name ? common_vendor.unref(endGps).name : "请选择你的目的地"),
        k: common_vendor.o(($event) => mapChooseLocation("目的地"))
      };
    };
  }
});
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "D:/a-django/专高六2/routine/qian1/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
