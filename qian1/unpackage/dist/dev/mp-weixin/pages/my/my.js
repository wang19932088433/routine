"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_uni_card2 = common_vendor.resolveComponent("uni-card");
  const _easycom_uv_cell2 = common_vendor.resolveComponent("uv-cell");
  const _easycom_uv_cell_group2 = common_vendor.resolveComponent("uv-cell-group");
  (_easycom_uni_card2 + _easycom_uv_cell2 + _easycom_uv_cell_group2)();
}
const _easycom_uni_card = () => "../../uni_modules/uni-card/components/uni-card/uni-card.js";
const _easycom_uv_cell = () => "../../uni_modules/uv-cell/components/uv-cell/uv-cell.js";
const _easycom_uv_cell_group = () => "../../uni_modules/uv-cell/components/uv-cell-group/uv-cell-group.js";
if (!Math) {
  (_easycom_uni_card + _easycom_uv_cell + _easycom_uv_cell_group)();
}
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  __name: "my",
  setup(__props) {
    let getPhone = common_vendor.ref("15412631254");
    let phone = common_vendor.computed(() => {
      let copyPhone = getPhone.value;
      if (getPhone) {
        copyPhone = copyPhone.slice(0, 3) + "****" + copyPhone.slice(7, 11);
      }
      return copyPhone;
    });
    let flag = common_vendor.ref(true);
    let avatarUrl = common_vendor.ref("");
    common_vendor.onMounted(() => {
      common_vendor.index.setNavigationBarTitle({ title: t("my.myTitle") });
      let lang = common_vendor.index.getStorageSync("lang") || "";
      if (lang && lang == "en") {
        flag.value = false;
      }
    });
    const { t } = common_vendor.useI18n();
    let coupon = common_vendor.computed(() => {
      return t("my.myItem.coupon");
    });
    let myInvoice = common_vendor.computed(() => {
      return t("my.myItem.myInvoice");
    });
    let serviceCenter = common_vendor.computed(() => {
      return t("my.myItem.serviceCenter");
    });
    let option = common_vendor.computed(() => {
      return t("my.myItem.option");
    });
    let join = common_vendor.computed(() => {
      return t("my.myItem.join");
    });
    let uploadAvatar = async () => {
      const chooseImageRes = await common_vendor.index.chooseImage({
        count: 1,
        // 限制选择一张
        sizeType: ["original", "compressed"],
        // 可以选择原图或压缩图
        sourceType: ["album", "camera"]
        // 从相册或相机选择
      });
      console.log(chooseImageRes);
      if (chooseImageRes.errMsg === "chooseImage:ok") {
        avatarUrl.value = chooseImageRes.tempFilePaths[0];
      }
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.unref(avatarUrl) ? common_vendor.unref(avatarUrl) : "https://profile-avatar.csdnimg.cn/827405f4711f457d81f1f2d84724340d_qq_41732757.jpg!1",
        b: common_vendor.o(
          //@ts-ignore
          (...args) => common_vendor.unref(uploadAvatar) && common_vendor.unref(uploadAvatar)(...args)
        ),
        c: common_vendor.t(common_vendor.unref(phone)),
        d: common_vendor.p({
          margin: "30rpx",
          padding: "12rpx",
          spacing: "12rpx",
          note: "Tips"
        }),
        e: common_vendor.p({
          border: false,
          title: common_vendor.unref(coupon),
          isLink: true,
          url: "/pages/my/coupon/Coupon"
        }),
        f: common_vendor.p({
          border: false,
          title: common_vendor.unref(myInvoice),
          isLink: true,
          url: "/pages/my/myInvoice/myInvoice"
        }),
        g: common_vendor.p({
          border: false,
          title: common_vendor.unref(serviceCenter),
          isLink: true,
          url: "/pages/my/serviceCenter/serviceCenter"
        }),
        h: common_vendor.p({
          border: false,
          title: common_vendor.unref(option),
          isLink: true,
          url: "/pages/my/options/options"
        }),
        i: common_vendor.p({
          border: false,
          title: common_vendor.unref(join),
          isLink: true,
          url: ""
        }),
        j: common_vendor.p({
          border: false
        }),
        k: common_vendor.p({
          margin: "30rpx",
          padding: "0rpx",
          spacing: "0rpx"
        })
      };
    };
  }
});
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2f1ef635"], ["__file", "D:/a-django/专高六2/routine/qian1/pages/my/my.vue"]]);
wx.createPage(MiniProgramPage);
