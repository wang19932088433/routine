
import Request from 'luch-request'

/**
 * @module  Utils-Request
 *
 * @description  创建请求实例方法.
 *
 * @param    {{}}   [options={}]  Default is `{}`
 *
 * @returns  {any}
 *
 * @export
 */

function createRequest(options = {}) {
  return new Request({
    ...options,
  })
}

// 创建http实例对象
/**
 * @description  创建请求实例.
 *
 * @type  {any}
 */
const http = createRequest({
  baseURL: 'http://127.0.0.1:3000',
})


//设置请求拦截器
http.interceptors.request.use(
  (config) => {
    // 可以在请求的时候固定设置content-type以及token等信息内容
    config.header = {
      'content-type': 'application/json',
      token: uni.getStorageSync('token') || '',
    }
    return config
  },
  (config) => {
    // 可使用async await 做异步操作
    return Promise.reject(config)
  },
)


//设置响应拦截器
// http.interceptors.response.use(
//   (response) => {
//     if (response.statusCode >= 200 && response.statusCode < 300) {
//       // 外部资源请求的时候进行特殊拦截，比如百度地图，可以通过response.status === 0来判断是否请求成功
//       // 这是需要通过接口去分析返回的数据结构
//       if (response.data.status === 0) {
//         return response.data.result
//       }

//       // 也可以将直接将response.data用户所需内容直接返回，组件中不需要每次都去操作data属性节点
//       if (response.data.code === 208) {
//         uni.removeStorageSync('token')
//         uni.reLaunch({
//           url: '/pages/login/login',
//         })
//         return false
//       }

//       if (response.data.code === 200) {
//         return response.data.data
//       }
//       return response.data
//     }
//     console.log('response=====', response)
//   },
//   (error) => {
//     // 利用http状态码可以实现不同情况接口错误内容的收集与反馈操作
//     if (error && error.errMsg) {
//       // 判断http请求的状态码，并设置不同的错误提示信息
//       switch (error.statusCode) {
//         case 400:
//           error.errMsg = '错误请求'
//           break
//         case 401:
//           error.errMsg = '未授权，请重新登录'
//           break
//         case 403:
//           error.errMsg = '拒绝访问'
//           break
//         case 404:
//           error.errMsg = '请求错误,未找到该资源'
//           break
//         case 405:
//           error.errMsg = '请求方法未允许'
//           break
//         case 408:
//           error.errMsg = '请求超时'
//           break
//         case 500:
//           error.errMsg = '服务器端出错'
//           break
//         case 501:
//           error.errMsg = '网络未实现'
//           break
//         case 502:
//           error.errMsg = '网络错误'
//           break
//         case 503:
//           error.errMsg = '服务不可用'
//           break
//         case 504:
//           error.errMsg = '网络超时'
//           break
//         case 505:
//           error.errMsg = 'http版本不支持该请求'
//           break
//         default:
//           error.errMsg = `连接错误`
//       }
//       const errorData = {
//         code: error.statusCode,
//         message: error.errMsg,
//       }
//       // 统一错误处理可以放这，例如页面提示错误...
//       console.log('统一错误处理: ', errorData)
//     }
//     return Promise.reject(error)
//   },
// )

export default http

