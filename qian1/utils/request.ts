export default (options)=>{
 return  new Promise((resolve,reject)=>{
   //提示框
   wx.showLoading({
     title: '数据请求中...',
   })
    wx.request({
      url:options.url ,//请求地址
      method:options.method,//请求方式
      data:options.data,//请求参数
      success(res){
        resolve(res)
      },
      fail(err){
        reject(err)
      },
      complete(){
        //关闭提示框
        wx.hideLoading()
      }
    })
 })
}